import 'package:m2_assessment_1_2/m2_assessment_1_2.dart' as m2_assessment_1_2;

void main(List<String> arguments) {
  var arr = [
    "Ambani Africa",
    "EasyEquities",
    "Takealot",
    "Shyft",
    "iiDENTIFii",
    "SISA",
    "Guardian Health Platform",
    "Murimi",
    "UniWise",
    "App",
    "Rekindle",
    "Hellopay softPOS",
    "Roadsave",
    "CheckersSixty60",
    "equities",
    "birdpro",
    "technishen",
    "matric-live",
    "stokfella",
    "bottles",
    "hearing",
    "league of legends",
    "examsta",
    "xitsonga dictionary",
    "greenfingers mobile",
    "guardian health",
    "my pregnancy journey",
    "Digger",
    "Realities",
    "Vula mobile",
    "Hydra farm",
    "Matric live",
    "Franc",
    "Over",
    "Loctransi",
    "Naked Insurance",
    "My pregnacy journey",
    "Loot defence",
    "Mowash",
    "cowa-bunga",
    "acgl",
    "Besmarta",
    "xander english",
    "ctrl",
    "pineapple",
    "bestee",
    "stokfella",
    "asi snakes",
    "TransUnion 1Check",
    "OrderIN",
    "EcoSlips",
    "InterGreatMe",
    "Zulzi",
    "Hey Jude",
    "Oru Social",
    "TouchSA",
    "Pick n Pay Super Animals 2",
    "The TreeApp South Africa",
    "WatIf Health Portal",
    "Awethu Project",
    "Shyft",
    "iKhoha",
    "HearZA",
    "Tuta-me",
    "KaChing",
    "Friendly Math Monsters for Kindergarten",
    "Wumdrop",
    "Vula mobile",
    "m4jam",
    "Eskomsepush",
    "DStv now",
    "Cput mobile",
    "Zapper",
    "Wildlife-tracker",
    "Vigo",
    "Sync mobile",
    "Supersport",
    "Rea vaya",
    "My belongings",
    "Live inspect",
    "Snapscan",
    "Price check",
    "Nedbank",
    "Markitshare",
    "kids aid",
    "Gautrain Buddy",
    "DStv",
    "Deloitte digital",
    "Bookly",
    "Transunion dealers guide",
    "Rapid Targets",
    "Plascon",
    "Pharazapp",
    "Matchy",
    "Healthaid",
    "FnB banking"
  ];

  var appWiner = [
    "cowa-bunga",
    "acgl",
    "Besmarta",
    "xander english",
    "ctrl",
    "pineapple",
    "bestee",
    "stokfella",
    "asi snakes",
    "TransUnion 1Check",
    "OrderIN",
    "EcoSlips",
    "InterGreatMe",
    "Zulzi",
    "Hey Jude",
    "Oru Social",
    "TouchSA",
    "Pick n Pay Super Animals 2",
    "The TreeApp South Africa",
    "WatIf Health Portal",
    "Awethu Project",
    "Shyft"
  ];

  //sorting of the array
  arr.sort();
  appWiner.sort();

//printing the array of the sorted list
  int numOfapps = 0;
  print(
      "A) This is the App of the year winners sorted in alphabetical order: \n");
  for (int x = 0; x < arr.length; x++) {
    print(arr[x] + "\n");
    numOfapps = x++;
  }

  //printing only the 2017 and 2018 winners
  print("\nB) Here is the winners from the years 2017 and 2018");
  for (int x = 0; x < appWiner.length; x++) {
    print(appWiner[x] + "\n");
  }

  //printing how many apps are in the array

  print("\n The total number of apps equals: " + numOfapps.toString());
}
