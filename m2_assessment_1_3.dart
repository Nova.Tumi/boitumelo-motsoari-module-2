import 'package:m2_assessment_1_3/m2_assessment_1_3.dart' as m2_assessment_1_3;

void main(List<String> arguments) {
  //declaration of variables
  var appAward = app("Checkers Sixty60", "The Best Enterprise Solution award",
      "Kimberley Taylor", "2020");
  //Printing out the class
  print("The app: " +
      appAward.appName +
      "won the " +
      appAward.yearWon +
      " MTN app of the year award in the category of: " +
      appAward.category +
      "\n The app was developed by: " +
      appAward.developer);

  //using the class function
  print("\n" + appAward.appNameCaps("Checkers Sixty60"));
}

class app {
  String appName = "";
  String category = "";
  String developer = "";
  String yearWon = "";

  //all functions
  app(String appN, String appCat, String appDev, String appWon) {
    appName = appN;
    category = appCat;
    developer = appDev;
    yearWon = appWon;
  }

  String appNameCaps(String appNm) {
    return appNm.toUpperCase();
  }
}
